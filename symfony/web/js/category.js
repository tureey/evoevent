$(document).ready(function(){

    // Abrimos y cerramos los submenus de navegación)
    $('#subcategories header').click(function()
    {
        // En caso de que el otro submenu esté desplegado lo cerramos
        if ($('#filters .container-responsive').is(':visible'))
        {
            $('#filters .container-responsive').slideUp('slow');
            setTimeout(function(){
                $('#filters i').removeClass('fa-minus').addClass('fa-plus')
            },400);
        }

        //$('#subcategories > div').delay(500).slideDown('slow');

        if($('#subcategories .container-responsive').is(':visible'))
        {
            $('#subcategories .container-responsive').slideUp('slow');
            setTimeout(function(){
                $('#subcategories i').removeClass('fa-minus').addClass('fa-plus')
            },400);
        }
        else
        {
            $('#subcategories .container-responsive').slideDown('slow');
            setTimeout(function() {
                $('#subcategories i').removeClass('fa-plus').addClass('fa-minus')
            },400);
        }
    });


    // Abrimos y cerramos los submenus de navegación)
    $('#filters header').click(function() {

        // En caso de que el otro submenu esté desplegado lo cerramos
        if ($('#subcategories .container-responsive').is(':visible'))
        {
            $('#subcategories .container-responsive').slideUp('slow');
            setTimeout(function(){
                $('#subcategories i').removeClass('fa-minus').addClass('fa-plus')
            },400);
        }

        if ($('#filters .container-responsive').is(':visible'))
        {
            $('#filters .container-responsive').slideUp('slow');
            setTimeout(function(){
                $('#filters i').removeClass('fa-minus').addClass('fa-plus')
            },400);
        }
        else
        {
            $('#filters .container-responsive').slideDown('slow');
            setTimeout(function(){
                $('#filters i').removeClass('fa-plus').addClass('fa-minus')
            },400);
        }
    });



    // Control sobre la activación del checkbox (solo uno)
    $('div.checkbox-option').find('input[type="checkbox"]').on('change', function() {
        $('input[type="checkbox"]').not(this).prop('checked', false);
    });



    // Inicializamos la barra de rango de precios
    $('.slider-input').jRange({
        from: 0,
        to: 100,
        step: 1,
        scale: [0,25,50,75,100],
        format: '%s',
        width: 300,
        showLabels: true,
        isRange : true
    });



    // Inicializamos ambos datePickers
    $('#fechaMax').datepicker({ dateFormat: 'yy-mm-dd' });

    $('#fechaMin').datepicker({ dateFormat: 'yy-mm-dd' }).bind("change",function(){

        var minValue    = $(this).val();
        minValue        = $.datepicker.parseDate("yy-mm-dd", minValue);

        minValue.setDate(minValue.getDate()+1);

        $("#fechaMax").datepicker( "option", "minDate", minValue );
    })
});
