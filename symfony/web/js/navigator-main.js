$(document).ready(function(){

    $('#nav-categories-header-responsive').click(function(){
        if($(window).innerWidth() < 992){
            if($('#categories-list').is(':visible')){
                $('#categories-list').slideUp();
                $('#nav-categories-header-responsive i').css('transform', 'rotate(0deg)');
            }
            else{
                $('#categories-list').slideDown();
                $('#nav-categories-header-responsive i').css('transform', 'rotate(180deg)');
            }

        }
    });
});