// Actualizamos la foto principal
$(document).ready(function(){
    // Actualizacion de las fotos via click
    $('#image-list li.image-list-element').click(function(){
        var src = $(this).find('img').attr('src');
         $('#image-active img').attr('src', src);
    });

    // Actualizacion de las fotos via click
    $('#image-list-responsive li.image-list-element').click(function(){
        var src = $(this).find('img').attr('src');
        $('#image-active-responsive img').attr('src', src);
    });
});

