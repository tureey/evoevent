<?php
/**
 * Created by PhpStorm.
 * User: tureey
 * Date: 1/06/16
 * Time: 22:54
 */

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class PetitionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'title',
            TextType::class,
            array(
                'label' => 'str_title',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'str_obligatorio')),
                )
            )
        );

        $builder->add(
            'message',
            TextareaType::class,
            array(
                'label' => 'str_message',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'str_obligatorio')),
                )
            )
        );
    }

    public function getName()
    {
        return 'petition';
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle/Entity/Petition',
            'csrf_protection'	 => true,
            'csrf_field_name'	 => '_token',
        ));
    }
}