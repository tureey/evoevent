<?php
/**
 * Created by PhpStorm.
 * User: Gurdeep
 * Date: 17/04/2016
 * Time: 22:30
 */

namespace AppBundle\Form;

use AppBundle\Entity\Category;
use AppBundle\Entity\CategoryRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;


class CategoryFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'name',
            TextType::class,
            array(
                'label' => 'str_title',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'str_obligatorio')),
                )
            )
        );

        $builder->add(
            'description',
            TextType::class,
            array(
                'label' => 'str_descripcion',
                'required' => false,
            )
        );

        $builder->add(
            'isPublic',
            CheckboxType::class,
            array(
                'label' => 'str_isPublic',
                'required' => false
            )
        );

        $builder->add(
            'isActive',
            CheckboxType::class,
            array(
                'label' => 'str_isActive',
                'required' => false
            )
        );

        $builder->add(
            'parentCategory',
            EntityType::class,
            array(
                'class' => 'AppBundle:Category',
                'choice_label' => 'name',
                'label' => 'str_parentCategory',
                'required' => false,
                'query_builder' => function (CategoryRepository $r)
                {
                    return $r->createQueryBuilder('s')
                        ->where('s.parentCategory is null');
                }
            )
        );

    }
}