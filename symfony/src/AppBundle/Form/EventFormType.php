<?php

namespace AppBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class EventFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'title',
            TextType::class,
            array(
                'label' => 'str_title',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'str_obligatorio')),
                )
            )
        );

        $builder->add(
            'description',
            TextareaType::class,
            array(
                'label' => 'str_descripcion',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'str_obligatorio')),
                )
            )
        );

        $builder->add(
            'location',
            TextType::class,
            array(
                'label' => 'str_localizacion',
                'required' => false
            )
        );

        $builder->add(
            'isLocationVisible',
            CheckboxType::class,
            array(
                'label' => 'str_isVisible',
                'required' =>false,
                'data' => true
            )
        );

        /**
         * Estas son las coordenadas de EVOLUTIONAGENTS
         */
        $builder->add(
            'mapX',
            HiddenType::class,
            array(
                'required' =>false,
                'data' => 41.37471
            )
        );

        $builder->add(
            'mapY',
            HiddenType::class,
            array(
                'required' =>false,
                'data' => 2.186282
            )
        );

        $builder->add(
            'website',
            UrlType::class,
            array(
                'label' => 'str_website',
                'required' => false,

            )
        );

        $builder->add(
            'isWebsiteVisible',
            CheckboxType::class,
            array(
                'label' => 'str_isVisible',
                'required' =>false,
                'data' => true
            )
        );

        $builder->add(
            'isPriceVisible',
            CheckboxType::class,
            array(
                'label' => 'str_isVisible',
                'required' =>false,
                'data' => true
            )
        );

        $builder->add(
            'price',
            MoneyType::class,
            array(
                'label' => 'str_price',
                'required' => false,
                'currency' => false,
            )
        );

        $builder->add(
            'date',
            DateTimeType::class,
            array(
                'label' => 'str_date',
                'required' => false
            )
        );

        $builder->add(
            'isDateVisible',
            CheckboxType::class,
            array(
                'label' => 'str_isVisible',
                'required' =>false,
                'data' => true
            )
        );

        $builder->add(
            'validDate',
            DateTimeType::class,
            array(
                'label' => 'str_validDate',
                'required' => false
            )
        );

        $builder->add(
            'isValidDateVisible',
            CheckboxType::class,
            array(
                'label' => 'str_isVisible',
                'required' =>false,
                'data' => true
            )
        );

        $builder->add(
            'isPublic',
            CheckboxType::class,
            array(
                'label' => 'str_isPublic',
                'required' =>false
            )
        );

        $builder->add(
            'isOutstanding',
            CheckboxType::class,
            array(
                'label' => 'str_destacados',
                'required' =>false
            )
        );

        $builder->add(
            'category',
            EntityType::class,
            array(
                'class'=>'AppBundle:Category',
                'choice_label'=>'name',
                'label' => 'str_categoria',
                'required' => array(
                    new NotBlank(array('message'=> 'str_obligatorio')),
                )
            )
        );

        $builder->add(
            'tags',
            EntityType::class,
            array(
                'class'=>'AppBundle:Tag',
                'choice_label'=>'name',
                'label' => 'str_tags',
                'required' => false,
                'multiple' => true,
                'expanded' => false,
            )
        );

        $builder->add(
            'purchaseOption',
            ChoiceType::class,
            array(
            'choices'  => array(
                'str_nothing' => 0,
                'str_reserve' => 1,
                'str_purchase' => 2,
            ),
        ));

        $builder->add(
            'isActive',
            CheckboxType::class,
            array(
                'label' => 'str_isActive',
                'required' => false
            )
        );

    }
    public function getName()
    {
        return 'event';
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle/Entity/Event',
            'csrf_protection'	 => true,
            'csrf_field_name'	 => '_token',
        ));
    }
}