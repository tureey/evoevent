<?php

namespace AppBundle\Form;


use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'name',
            TextType::class,
            array(
                'label' => 'str_nombre',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'str_obligatorio')),
                )
            )
        );

        $builder->add(
            'surname',
            TextType::class,
            array(
                'label' => 'str_surname',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'str_obligatorio')),
                )
            )
        );

        $builder->add(
            'userName',
            TextType::class,
            array(
                'label' => 'str_nickname',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'str_obligatorio')),
                )
            )
        );


        $builder->add(
            'email',
            TextType::class,
            array(
                'label' => 'str_email',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'str_obligatorio')),
                )
            )
        );

        $builder->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password'),
                'required' => true,
            )
        );

        $builder->add(
            'shipName',
            TextType::class,
            array(
                'label' => 'str_shipname',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'str_obligatorio')),
                )
            )
        );

        $builder->add(
            'shipRank',
            TextType::class,
            array(
                'label' => 'str_shiprank',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'str_obligatorio')),
                )
            )
        );
    }

    public function getName()
    {
        return 'user';
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle/Entity/Users',
            'csrf_protection'	 => true,
            'csrf_field_name'	 => '_token',
        ));
    }
}