<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\EntityManager;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use \Mailgun\Mailgun;

class SearchController extends BaseController{

    /**
     * @Route ("/search/{param}", name="search")
     */
    public function getSearch(Request $request, $param){

        if(!$param){
            $message = "ERROR no se ha introducido nada";
            print_r($message);
            die();
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            'SELECT e
            FROM AppBundle:Event e
            WHERE e.title LIKE :param'
        )->setParameter('param', '%'.$param.'%');

        $events = $query->getResult();


        $this->addData('res', $events);
        $this->addData('titulo', 'search');
        return $this->render('AppBundle:search:search.html.twig', $this->getData());
    }

}