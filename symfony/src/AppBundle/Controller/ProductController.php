<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Comments;
use AppBundle\Entity\Event;
use AppBundle\Entity\Historic;
use AppBundle\Entity\Users;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\EntityManager;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

class ProductController extends BaseController{

    /**
     * @Route ("/product/{id}", name="producto")
     */
    public function getIndex($id){

        $em = $this->getDoctrine()->getManager();

        $event = $em->getRepository('AppBundle:Event')->find($id);

        if(!$event instanceof Event){
            $message="Evento incorrecto";
            print_r($message);
            die();
        }

        $category = $event->getCategory();
        if(!$event->getIsActive() || !$category->getIsActive()){
            return $this->redirectToRoute('index');
        }

        $this->puedeVer($event);
        $this->addData('titulo', 'Product page');
        $this->addData('event', $event);
        return $this->render('AppBundle:product:product.html.twig', $this->getData());
    }

    /**
     * @Route ("/product/buy/{id}", name="purchase_product")
     */
    public function buyProduct($id){

        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $em = $this->getDoctrine()->getManager();

            $event = $em->getRepository('AppBundle:Event')->find($id);

            if (!$event instanceof Event) {
                $message = "Evento incorrecto";
                print_r($message);
                die();
            }

            $userId = $this->get('security.token_storage')->getToken()->getUser()->getId();
            $user = $em->getRepository('AppBundle:Users')->find($userId);

            $historic = new Historic();
            // Provisional, faltaria hacer el modal
            $historic->setQuantity(1);
            $em->persist($historic);

            //guardamos los datos del historico
            $historic->setEvent($event);
            $historic->setUser($user);
            $historic->setDate(new \DateTime());

            $this->setTagsToUser($user, $event);
            $user->addHistoric($historic);
            $em->persist($user);
            $em->flush();

            $this->sendResponseStatus('OK');

            // Generamos los datos para la respuesta ajax
            return new JSONResponse($this->getData());
        }else{
            $message="Tienes que estar registrado para comprar/reservar un evento";
            print_r($message);
            die();
        }
    }

    /**
     * @Route ("/product/{id}/new/comment", name="new_comment")
     */
    public function newComment(Request $request, $id){

        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            // authenticated REMEMBERED, FULLY will imply REMEMBERED (NON anonymous)

            $em = $this->getDoctrine()->getManager();

            $event = $em->getRepository('AppBundle:Event')->find($id);

            if(!$event instanceof Event){
                $message="Evento incorrecto";
                print_r($message);
                die();
            }

            $titulo = $request->get('titulo');
            $contenido = $request->get('contenido');
            $valoracion = $request->get('valoracion');

            if(empty($titulo) || empty($contenido) || empty($valoracion) || (int) $valoracion > 5 || (int) $valoracion < 1){
                $message="ERROR datos incorrectos";
                print_r($message);
                die();
            }

            $user = $this->get('security.token_storage')->getToken()->getUser();

            $usuario = $em->getRepository('AppBundle:Users')->find($user->getId());

            if(!$usuario instanceof Users){
                $message="ERROR usuario incorrecto";
                print_r($message);
                die();
            }

            $comment = new Comments();

            $em->persist($comment);

            $comment->setTitulo($titulo);
            $comment->setContent($contenido);
            $comment->setValoracion($valoracion);
            $comment->setDate(new \DateTime());
            $comment->setUser($usuario);
            $comment->setEvent($event);

            $event->addComment($comment);
            $usuario->addComment($comment);

            $em->flush();


        }else{
            $message="Tienes que estar logueado para escribir un mensaje";
            print_r($message);
            die();
        }


        $this->puedeVer($event);
        $this->addData('titulo', 'Product page');
        $this->addData('event', $event);
        return $this->redirectToRoute('producto', array('id'=>$event->getId()));
    }

    /**
     * funcion que añade los tags del evento que no tenga el usuario, asi las recomendaciones seran mas apuradas
     */
    private function setTagsToUser(Users $user, Event $event){
        $eventTags = $event->getTags()->getValues();

        $userTags = $user->getTags()->getValues();

        $newTags = array_udiff($eventTags, $userTags, function ($obj_a, $obj_b) {
            return $obj_a->getId() - $obj_b->getId();
        });

        foreach($newTags as $tag){
            $user->addTag($tag);
        }
    }

    public function puedeVer(Event $event){
        if(!$event->getIsPublic()){
            $securityContext = $this->container->get('security.authorization_checker');
            if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
                $message="Evento privado, registrate para ver esta categoria";
                print_r($message);
                die();
            }
        }
    }


}