<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\EntityManager;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use \Mailgun\Mailgun;

class IndexController extends BaseController{

    /**
     * @Route ("/", name="index")
     */
    public function getIndex(){

        $em = $this->getDoctrine()->getManager();

        //comprobamos si esta logeado o no
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            // authenticated REMEMBERED, FULLY will imply REMEMBERED (NON anonymous)
            $user = $this->get('security.token_storage')->getToken()->getUser();

            $destacados = $em->getRepository('AppBundle:Event')->findBy(array('isOutstanding'=>true, 'isActive'=>true), array('id' => 'DESC'), 4);

            $ultimos = $em->getRepository('AppBundle:Event')->findBy(array('isActive'=>true), array('id' => 'DESC'), 4);

            if($securityContext->isGranted('ROLE_USER')){
                $recomendados = $this->getRecomended($user);

                $this->addData('recomendados', $recomendados);
            }


        }else{
            $destacados = $em->getRepository('AppBundle:Event')->findBy(array('isOutstanding'=>true, 'isPublic' => true, 'isActive'=>true), array('id' => 'DESC'), 4);
            $ultimos = $em->getRepository('AppBundle:Event')->findBy(array('isPublic' => true, 'isActive'=>true), array('id' => 'DESC'), 4);
        }

        $this->addData('destacados', $destacados);
        $this->addData('ultimos', $ultimos);
        $this->addData('titulo', 'index');
        return $this->render('AppBundle:index:index.html.twig', $this->getData());
    }

    //funciones para cambiar el idioma del usuario

    /**
     * @Route ("/set/locale/{locale}", name="set_locale")
     *
     *  @param Request $request
     *
     * @return Response
     */

    public function setLocale(Request $request, $locale){

        $session = $request->getSession();

        //comprobamos que el idioma es del tipo que tenemos definidos, de momento en y es
        if($locale != 'en' && $locale != 'es'){
            $message = "ERROR incorrect language";
            print_r($message);
            die();
        }

        //comprobamos si esta logeado o no
        $securityContext = $this->container->get('security.authorization_checker');

        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {

            // authenticated REMEMBERED, FULLY will imply REMEMBERED (NON anonymous)
            $em = $this->getDoctrine()->getManager();

            if($securityContext->isGranted('ROLE_USER')){
                $user = $this->get('security.token_storage')->getToken()->getUser();

                $usuario = $em->getRepository('AppBundle:Users')->find($user->getId());
                $usuario->setLocale($locale);
            }else if($securityContext->isGranted('ROLE_ADMIN')){
                $admin = $this->get('security.token_storage')->getToken()->getUser();

                $admin = $em->getRepository('AppBundle:Admin')->find($admin->getId());
                $admin->setLocale($locale);
            }

            $em->flush();

            $session->set('_locale', $locale);
        }else{
            $session->set('_locale', $locale);
        }


        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }

    /**
     * @Route ("/testMail", name="mails")
     */
    public function testMail(){

      # Include the Autoloader (see "Libraries" for install instructions)


      # Instantiate the client.
      $client = new \Http\Adapter\Guzzle6\Client();
      $mgClient = new Mailgun('key-0d3f7e1fcf3bdcfb6d0f19d0cec8b80f', $client);
      $domain = "sandbox67ac6b9040d8488cbd3b9ff7ffbcc155.mailgun.org";

      # Make the call to the client.
      $result = $mgClient->sendMessage("$domain",
                    array('from'    => 'Mailgun Sandbox <postmaster@sandbox67ac6b9040d8488cbd3b9ff7ffbcc155.mailgun.org>',
                          'to'      => 'Albert <univers1000@gmail.com>',
                          'subject' => 'Hello Albert',
                          'text'    => 'Congratulations Albert, you just sent an email with Mailgun!  You are truly awesome!  You can see a record of this email in your logs: https://mailgun.com/cp/log .  You can send up to 300 emails/day from this sandbox server.  Next, you should add your own domain so you can send 10,000 emails/month for free.'));
    }


}
