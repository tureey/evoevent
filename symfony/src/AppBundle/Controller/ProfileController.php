<?php
namespace AppBundle\Controller;

use AppBundle\Entity\FileEvo;
use AppBundle\Form\AdminFormType;
use AppBundle\Form\ChangePasswordType;
use AppBundle\Form\Model\ChangePassword;
use AppBundle\Form\UserFormType;
use AppBundle\Form\EventFormType;
use AppBundle\Form\CategoryFormType;
use AppBundle\Form\TagFormType;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\EntityManager;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Admin;
use AppBundle\Entity\Users;
use AppBundle\Entity\Category;
use AppBundle\Entity\Event;
use AppBundle\Entity\Tag;

class ProfileController extends BaseController
{

    /**
     * @Route ("/profile", name="profile")
     */
    public function getIndex(Request $request)
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $em = $this->getDoctrine()->getManager();

            $user = $this->get('security.token_storage')->getToken()->getUser();

            $allTags = $em->getRepository('AppBundle:Tag')->findAll();

            $userTags = $user->getTags()->getValues();

            $allTags = array_udiff($allTags, $userTags, function ($obj_a, $obj_b) {
                return $obj_a->getId() - $obj_b->getId();
            });

            $changePasswordModel = new ChangePassword();
            $form = $this->createForm(ChangePasswordType::class, $changePasswordModel);


            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $usuario = $em->getRepository('AppBundle:Users')->find($user->getId());

                $hash = $this->get('security.password_encoder')->encodePassword($usuario, $form->get('newPassword')->getData());

                $usuario->setPassword($hash);

                $em->persist($usuario);

                $em->flush();
                return $this->redirect($this->generateUrl('profile'));
            }


            $this->addData('form', $form->createView());
            $this->addData('user', $user);
            $this->addData('allTags', $allTags);
            return $this->render('AppBundle:profile:profile.html.twig', $this->getData());
        }

        return $this->redirect("/");
    }

    /**
     * @Route ("/profile/add/tag/{id}", name="profile_add_tag")
     */
    public function addTag($id)
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {

            $em = $this->getDoctrine()->getManager();

            $userId = $this->get('security.token_storage')->getToken()->getUser()->getId();

            $user = $em->getRepository('AppBundle:Users')->find($userId);

            if(!$user instanceof Users){
                $message="Usuario incorrecta";
                print_r($message);
                die();
            }

            $tag = $em->getRepository('AppBundle:Tag')->find($id);

            if(!$tag instanceof Tag){
                $message="Tag incorrecta";
                print_r($message);
                die();
            }

            $userTags = $user->getTags()->getValues();

            if(in_array($tag, $userTags)){
                $message="Este usuario ya tiene este tag introducido";
                print_r($message);
                die();
            }

            $user->addTag($tag);

            $em->persist($user);

            $em->flush();


            $this->sendResponseStatus('OK');

            // Generamos los datos para la respuesta ajax
            return new JSONResponse($this->getData());
        }else{
            $message="Has de estar logueado para añadir tags";
            print_r($message);
            die();
        }
    }

    /**
     * @Route ("/profile/delete/tag/{id}", name="profile_delete_tag")
     */
    public function deleteTag($id)
    {
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {

            $em = $this->getDoctrine()->getManager();

            $userId = $this->get('security.token_storage')->getToken()->getUser()->getId();

            $user = $em->getRepository('AppBundle:Users')->find($userId);

            if(!$user instanceof Users){
                $message="Usuario incorrecta";
                print_r($message);
                die();
            }

            $tag = $em->getRepository('AppBundle:Tag')->find($id);

            if(!$tag instanceof Tag){
                $message="Tag incorrecta";
                print_r($message);
                die();
            }

            $userTags = $user->getTags()->getValues();

            if(in_array($tag, $userTags)){
                $user->removeTag($tag);
            }else{
                $message="Error este usuario no tiene este tag";
                print_r($message);
                die();
            }

            $em->persist($user);

            $em->flush();


            $this->sendResponseStatus('OK');

            // Generamos los datos para la respuesta ajax
            return new JSONResponse($this->getData());
        }else{
            $message="Has de estar logueado para añadir tags";
            print_r($message);
            die();
        }
    }

}