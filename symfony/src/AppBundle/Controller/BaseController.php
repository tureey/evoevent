<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/*
 * Controllador base de nuestra aplicacion
 */

class BaseController extends Controller{
    /**
     * @var $data
     */
    protected $data;

    /**
     * Enviar estado para el handleReponse de Javascript
     */
    protected function sendResponseStatus($status)
    {
        $all_status = array('OK', 'ERROR', 'WARNING', ' NO_AUTHORIZED');

        if (!in_array($status, $all_status)) {
            $status = 'ERROR';
        }

        $this->addData('type', $status);
    }

    /**
     *  Añade datos que se enviarán a la vista
     * @param string $key
     * @param $value
     */
    protected function addData($key, $value)
    {
        $this->data[$key] = $value;
    }

    /**
     * Devuelve los datos que se enviarán a la vista.Funcion getData
     *
     * @return array
     */
    protected function getData ()
    {
        return $this->data;
    }

    /**
     * Devuelve los eventos recomendados dependiendo de la vista y de los tags del usuario
     *
     * @param Category $category
     * @param Users $user
     * @return array
     */

    protected function getRecomended(Users $user, Category $category = null){

        $userTags = $user->getTags()->getValues();
        $em = $this->getDoctrine()->getManager();

        //cargamos los repositorios
        $events = $em->getRepository('AppBundle:Event');

        //esta funcion del repositorio de event nos devuelve los eventos que contienen los tags del usuario y si estamos en alguna categoria los relacionados con la categoria
        $eventsFilter = $events->getEventsFiltered($em, $userTags, $category);

        if(count($em->getRepository('AppBundle:Historic')->findAll())>0){
            $eventsMostPursached = array();

            //introducimos en el array $eventsMostPursached el porcentage de compra del evento relativo a todas las compras de la aplicacion la key es la id del evento y el value es el porcentage de compra
            foreach($eventsFilter as $event){
                $eventsMostPursached[$event->getId()] = (count($em->getRepository('AppBundle:Historic')->findBy(array('event'=>$event->getId())))/count($em->getRepository('AppBundle:Historic')->findAll()))*100;
            }

            //ordenamos el array de mayor a menor
            arsort($eventsMostPursached);

            //cogemos los 10 mas comprados de toda la aplicacion correspondiente a los tags del usuario y en caso de tener categoria a la categoria tambien
            $eventsMostPursached = array_slice($eventsMostPursached, 0, 10, true);

            $eventsReviews = array();

            //vamos a calcular la valoracion media de cada evento y ordenarlos segun su valoracion
            foreach($eventsMostPursached as $key => $value){
                $eventComents = $em->getRepository('AppBundle:Event')->find($key)->getComments()->getValues();

                $valoracionesTotal = 0;
                $valoracionMedia = 0;

                if(count($eventComents)>0){
                    foreach($eventComents as $comment){
                        $valoracionesTotal += $comment->getValoracion();
                    }
                    $valoracionMedia = $valoracionesTotal/count($eventComents);
                }else{
                    $valoracionMedia = 0;
                }

                $eventsReviews[$key] = $valoracionMedia;
            }

            arsort($eventsReviews);

            //cogemos los 4 mejor valorados
            $eventsReviews = array_slice($eventsReviews, 0, 4, true);

            $recomendedEvents = array();

            //cogemos los eventos
            foreach($eventsReviews as $eventId => $value){
                $recomendedEvents[] = $em->getRepository('AppBundle:Event')->find($eventId);
            }

            //devolvemos los eventos
            return $recomendedEvents;

        }else{
            return $eventsFilter;
        }

    }
}