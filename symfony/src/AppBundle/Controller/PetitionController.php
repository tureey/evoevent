<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Petition;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\EntityManager;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Form\PetitionFormType;



class PetitionController extends BaseController{

    /**
     * @Route ("/send/petition", name="send_petition")
     * Se renderiza un formulario de Petition para meterlo en la vista
     */
    public function renderPetitionAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $securityContext = $this->container->get('security.authorization_checker');

        if ($securityContext->isGranted('ROLE_USER')) {

            // authenticated REMEMBERED, FULLY will imply REMEMBERED (NON anonymous)
            $petition = new Petition();

            $em->persist($petition);

            $form = $this->createForm(PetitionFormType::class, $petition);

            $form->handleRequest($request);


            //comprovacion de ficheros
            if ($form->isSubmitted() && $form->isValid()) {

                $user = $this->get('security.token_storage')->getToken()->getUser();
                $user->addPetition($petition);
                $petition->setUser($user);

                $em->persist($petition);
                $em->flush();


                return $this->redirectToRoute('index');
            }

            $this->addData('formPetition', $form->createView());

            return $this->render('AppBundle:petition:sendPetition.html.twig', $this->getData());
        }

        else
            return $this->redirectToRoute('index');
    }
}