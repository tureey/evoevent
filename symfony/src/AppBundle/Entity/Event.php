<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
/**
 * @ORM\Entity
 * @ORM\Table(name="event")
 * @ORM\Entity(repositoryClass="EventRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Event
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $location;

    /**
     * Indica si la localizacion de un evento se muestra o no
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isLocationVisible;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $website;

    /**
     * Indica si la web de un evento se muestra o no
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isWebsiteVisible;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     */
    private $price;

    /**
     * Indica si el precio de un evento se muestra o no
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPriceVisible;


    /**
     * Fecha de realización del evento
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date;

    /**
     * Indica si la fecha de realizacion de un evento se muestra o no
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDateVisible;

    /**
     * Fecha de 'expiracion', pasada esta fecha el evento no se mostrará en la applicacion para los clientes
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $validDate;

    /**
     * Indica si la fecha de caducidad/validez de un evento se muestra o no
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isValidDateVisible;

    /**
     * Indica si un evento es publico o no, 1/yes en caso de publico, 0/no en caso de privado
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPublic;

    /**
     * Indica si un evento es destacado o no, 1/yes en caso de publico, 0/no en caso de privado
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isOutstanding;

    /**
     * Indica la cordenada X en el Maps
     * @ORM\Column(type="float")
     */
    private $mapX;

    /**
     * Indica la cordenada Y en el Maps
     * @ORM\Column(type="float")
     */
    private $mapY;

    /**
     * Indica las opciones que tiene el usuario para interactuar con el evento
     * 0 No se puede ni comprar ni reservar
     * 1 Se puede reservar
     * 2 Se puede comprar
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $purchaseOption;


    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\NotBlank()
     */
    private $category;


    /**
     * @ORM\OneToMany(targetEntity="FileEvo", mappedBy="event", cascade={"persist"})
     */
    private $files;

    /**
     * @ORM\OneToMany(targetEntity="Comments", mappedBy="event", cascade={"persist"})
     */
    private $comments;


    /**
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="events")
     * @ORM\JoinTable(name="events_tags")
     */
    private $tags;


    /**
     * Indica si el evento esta activo o no
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActive;


    /**
     * @var ArrayCollection
     */
    private $uploadedFiles;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->uploadedFiles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comments  = new \Doctrine\Common\Collections\ArrayCollection();

        // Marcamos como visibles por defectos todos los campos
        $this->isDateVisible = true;
        $this->isValidDateVisible = true;
        $this->isLocationVisible = true;
        $this->isWebsiteVisible = true;
        $this->mapX = 41.37471;
        $this->mapY = 2.186282;
    }

    /**
     * @return mixed
     */
    public function getUploadedFiles()
    {
        return $this->uploadedFiles;
    }

    /**
     * @param mixed $uploadedFiles
     */
    public function setUploadedFiles($uploadedFiles)
    {
        $this->uploadedFiles = $uploadedFiles;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return mixed
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param mixed $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getIsPriceVisible()
    {
        return $this->isPriceVisible;
    }

    /**
     * @param mixed $isPriceVisible
     */
    public function setIsPriceVisible($isPriceVisible)
    {
        $this->isPriceVisible = $isPriceVisible;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getValidDate()
    {
        return $this->validDate;
    }

    /**
     * @param mixed $validDate
     */
    public function setValidDate($validDate)
    {
        $this->validDate = $validDate;
    }

    /**
     * @return mixed
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * @param mixed $isPublic
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $categories
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param mixed $files
     */
    public function setFiles($files)
    {
        $this->files = $files;
    }


    public function addFile(FileEvo $file){
        $this->files[] = $file;
    }

    /**
     * @return mixed
     */
    public function getMapX()
    {
        return $this->mapX;
    }

    /**
     * @param mixed $mapX
     */
    public function setMapX($mapX)
    {
        $this->mapX = $mapX;
    }

    /**
     * @return mixed
     */
    public function getMapY()
    {
        return $this->mapY;
    }

    /**
     * @param mixed $mapY
     */
    public function setMapY($mapY)
    {
        $this->mapY = $mapY;
    }
    /**
     * @return mixed
     */
    public function getIsLocationVisible()
    {
        return $this->isLocationVisible;
    }


    /**
     * @param mixed $isLocationVisible
     */
    public function setIsLocationVisible($isLocationVisible)
    {
        $this->isLocationVisible = $isLocationVisible;
    }

    /**
     * @return mixed
     */
    public function getIsWebsiteVisible()
    {
        return $this->isWebsiteVisible;
    }

    /**
     * @param mixed $isWebsiteVisible
     */
    public function setIsWebsiteVisible($isWebsiteVisible)
    {
        $this->isWebsiteVisible = $isWebsiteVisible;
    }

    /**
     * @return mixed
     */
    public function getIsDateVisible()
    {
        return $this->isDateVisible;
    }

    /**
     * @param mixed $isDateVisible
     */
    public function setIsDateVisible($isDateVisible)
    {
        $this->isDateVisible = $isDateVisible;
    }

    /**
     * @return mixed
     */
    public function getIsValidDateVisible()
    {
        return $this->isValidDateVisible;
    }

    /**
     * @param mixed $isValidDateVisible
     */
    public function setIsValidDateVisible($isValidDateVisible)
    {
        $this->isValidDateVisible = $isValidDateVisible;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    public function addTags($tag){
        $tag->addEvent($this); // synchronously updating inverse side
        $this->tags[] = $tag;
    }

    /**
     * @return mixed
     */
    public function getPurchaseOption()
    {
        return $this->purchaseOption;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @param mixed $purchaseOption
     */
    public function setPurchaseOption($purchaseOption)
    {
        $this->purchaseOption = $purchaseOption;
    }





    public function upload( )
    {

        $valid_imagetypes = array("png", "jpg", "jpeg");
        $valid_doctype=array("pdf","doc", "txt");
        $minImage=false;

        foreach ($this->uploadedFiles as $uploadedFile)
        {
            $file_type = $uploadedFile->guessExtension();
            // comprovamos  de que tipos de ficheros se trata
            if (in_array(strtolower($file_type), $valid_imagetypes)){
                $isImage = true;
            }
            else if (in_array(strtolower($file_type), $valid_doctype)){
                $isImage=false;
            } else {
                $message = "Tipo de fichero erroneó";
                print_r($message);
                die();
                //aqui borrar los ficheros añadidos de la base de datos
            }

            if (($uploadedFile->getClientSize() <= 600000 && $isImage)|| ($uploadedFile->getClientSize() <= 948178718 && $isImage==false) ) {
                if (in_array(strtolower($file_type), $valid_imagetypes)) {
                    $minImage = true;
                }
            } else {
                $message = "Tamaño del fichero" . $uploadedFile->getClientOriginalName() ." supera el  límite máximo permitido";
                print_r($message);
                die();
            }
        }

        if ($minImage) {
            foreach ($this->uploadedFiles as $uploadedFile) {

                $file = new FileEvo();

                if (in_array(strtolower($file_type), $valid_imagetypes)){
                    $file->setType('image');
                }
                else if (in_array(strtolower($file_type), $valid_doctype)){
                    $file->setType('document');
                }


                /*
                 * These lines could be moved to the File Class constructor to factorize
                 * the File initialization and thus allow other classes to own Files
                 */
                $timeStamp = new \DateTime();
                $file_name = md5($timeStamp->getTimestamp()). '.' . $uploadedFile->guessExtension();
                $path = $file->getFileStoragePath() . '/' . $file_name;
                $file->setPath($path);
                $file->setSize($uploadedFile->getClientSize());
                $file->setName($file_name);
                $uploadedFile->move($file->getFileStoragePath(), $path);

                $this->addFile($file);
                $file->setEvent($this);

                unset($uploadedFile);
            }
        }
        else {
            $message = "No hay ninguna imagen";
            print_r($message);
            die();
        }
    }

    /**
     * @return mixed
     */
    public function getIsOutstanding()
    {
        return $this->isOutstanding;
    }

    /**
     * @param mixed $isOutstanding
     */
    public function setIsOutstanding($isOutstanding)
    {
        $this->isOutstanding = $isOutstanding;
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/files';
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param mixed $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @param Comments $comment
     */
    public function addComment($comment)
    {
        $this->comments[] = $comment;
    }


}